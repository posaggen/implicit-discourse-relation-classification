import os
import nltk
import pickle
import timeit

def save_classifier(classifier):
    f = open('my_classifier.pickle', 'wb')
    pickle.dump(classifier, f, -1)
    f.close()

def load_classifier():
    f = open('my_classifier.pickle', 'rb')
    classifier = pickle.load(f)
    f.close()
    return classifier

def getTrain():
    fin = open('train.txt')
    data = fin.readlines()
    train=[]
    for i in data:
        t = i.strip().split('\t')
        feature, label = t[0], t[1]
        flen = len(feature)
        dic = {}
        for j in range(flen):
            dic[j] = feature[j]
        train.append((dic, label))
    fin.close()
    return train

def getTest():
    fin = open('test.txt')
    data = fin.readlines()
    test = []
    for i in data:
        t = i.strip().split('\t')
        feature, label = t[0], t[1]
        flen = len(feature)
        dic = {}
        for j in range(flen):
            dic[j] = feature[j]
        test.append((dic, label))
    fin.close()
    return test

flog = open('log', 'a')
train = getTrain()
begin = timeit.default_timer()
classifier = nltk.classify.MaxentClassifier.train(train, 'IIS', trace = 3, max_iter=300)
end = timeit.default_timer()
print 'time: ', end - begin

test = getTest()
total = 0
right = 0
for t in test:
    total += 1
    result = classifier.classify(t[0])
    flog.write(result + '\n')
    if result == t[1]:
        right += 1
flog.write(str(right) + '\n')
flog.write(str(total) + '\n')
flog.write(str(float(right) / float(total)))

save_classifier(classifier)
