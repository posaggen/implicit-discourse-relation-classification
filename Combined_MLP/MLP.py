import six.moves.cPickle as pickle
import gzip
import os
import sys
import timeit
import random
import dill

import numpy as np

import theano
import theano.tensor as T

class LogisticRegression(object):

    def __init__(self, input, n_in, n_out):
        self.W = theano.shared(
            value=np.zeros(
                (n_in, n_out),
                dtype=theano.config.floatX
            ),
            name='W',
            borrow=True
        )
        self.b = theano.shared(
            value=np.zeros(
                (n_out,),
                dtype=theano.config.floatX
            ),
            name='b',
            borrow=True
        )
        self.p_y_given_x = T.nnet.softmax(T.dot(input, self.W) + self.b)
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)
        self.params = [self.W, self.b]
        self.input = input

    def negative_log_likelihood(self, y):
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

class HiddenLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh):
        self.input = input
        if W is None:
            W_values = np.asarray(
                rng.uniform(
                    low=-np.sqrt(6. / (n_in + n_out)),
                    high=np.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = np.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)
        self.W = W
        self.b = b
        lin_output = T.dot(input, self.W) + self.b
        self.output = (
            lin_output if activation is None
            else activation(lin_output)
        )
        self.params = [self.W, self.b]


class MLP(object):

    def __init__(self, rng, input, n_in, n_hidden, n_out):

        self.hiddenLayer = HiddenLayer(
            rng=rng,
            input=input,
            n_in=n_in,
            n_out=n_hidden,
            activation=T.tanh
        )
        self.logRegressionLayer = LogisticRegression(
            input=self.hiddenLayer.output,
            n_in=n_hidden,
            n_out=n_out
        )
        self.L1 = (
            abs(self.hiddenLayer.W).sum()
            + abs(self.logRegressionLayer.W).sum()
        )
        self.L2_sqr = (
            (self.hiddenLayer.W ** 2).sum()
            + (self.logRegressionLayer.W ** 2).sum()
        )
        self.negative_log_likelihood = (
            self.logRegressionLayer.negative_log_likelihood
        )
        self.params = self.hiddenLayer.params + self.logRegressionLayer.params
        self.input = input


def check(s):
#    if s == 'Contingency.Cause' or s == 'Comparison.Contrast' or s == 'Expansion.Conjunction' or s == 'Expansion.Restatement':
    if s == 'Contingency.Cause': 
        return True
    return False

def load_data(inpath, vec_len, doShuffle):
    global labeldic
    fin = open(inpath)
    L = fin.readlines()
    if doShuffle:
        random.shuffle(L)
    train_set = [[], []]
    now = 0
    for t in L:
        train_set[0].append([])
        tmp = t.strip().split('\t')
        train_set[1].append(labeldic[tmp[-1]])
        for i in range(vec_len):
            train_set[0][now].append(int(tmp[i]))
        now += 1
    def shared_dataset(data_xy, borrow = True):
        data_x, data_y = data_xy
        shared_x = theano.shared(np.asarray(data_x, dtype = theano.config.floatX), borrow = borrow)
        shared_y = theano.shared(np.asarray(data_y, dtype = theano.config.floatX), borrow = borrow)
        return shared_x, T.cast(shared_y, 'int32')

    train_set_x, train_set_y = shared_dataset(train_set)
    res = (train_set_x, train_set_y)
    return res

def load_gold(inpath, vec_len):
    fin = open(inpath)
    L = fin.readlines()
    res = []
    for i in L:
        tmp = i.strip().split('\t')[vec_len : ]
        res.append(tmp)
    return res

def load_test(inpath, vec_len):
    global labeldic
    fin = open(inpath)
    L = fin.readlines()
    test_set = []
    now = 0
    for t in L:
        test_set.append([])
        tmp = t.strip().split('\t')
        for i in range(vec_len):
            test_set[now].append(int(tmp[i]))
        now += 1
    def shared_dataset(data, borrow = True):
        return theano.shared(np.asarray(data, dtype = theano.config.floatX), borrow = borrow)

    test_set_x = shared_dataset(test_set)
    return test_set_x

def test_mlp(learning_rate=0.01, L1_reg=0.00, L2_reg=0.0001, n_epochs=10,
             batch_size=20, n_hidden=400, vec_len = 1200):
    datasets = load_data('train_.txt', vec_len, False)
    testsets = load_data('test_.txt', vec_len, False)
    gold = load_gold('test_.txt', vec_len)

    train_set_x, train_set_y = datasets
    test_set_x, test_set_y = testsets

    n_train_batches = train_set_x.get_value(borrow = True).shape[0] // batch_size

    print('... building the pre-model')

    index = T.lscalar()
    x = T.matrix('x')
    y = T.ivector('y')

    rng = np.random.RandomState(9264326)

    classifier0 = MLP(
        rng = rng,
        input = x,
        n_in = vec_len,
        n_hidden = n_hidden,
        n_out = 11
    )
    
#    classifier0 = dill.load(open("classifier0"))

    cost0 = (
        classifier0.negative_log_likelihood(y)
        + L1_reg * classifier0.L1
        + L2_reg * classifier0.L2_sqr
    )

    test_model0 = theano.function(
        inputs=[index],
        outputs=classifier0.logRegressionLayer.y_pred,
        givens={
            x: test_set_x,
        },
        on_unused_input='ignore'
    )

    gparams0 = [T.grad(cost0, param) for param in classifier0.params]

    updates0 = [
        (param, param - learning_rate * gparam)
        for param, gparam in zip(classifier0.params, gparams0)
    ]

    train_model0 = theano.function(
        inputs=[index],
        outputs=cost0,
        updates=updates0,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )
    print('... training pre-model')

    test_frequency = n_train_batches

    best_precision = 0
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False

    while (epoch < 9):
        epoch = epoch + 1
        for minibatch_index in range(n_train_batches):
            minibatch_avg_cost = train_model0(minibatch_index)
            iter = (epoch - 1) * n_train_batches + minibatch_index
            if (iter + 1) % test_frequency == 0:
                result = test_model0(0)
                total = 0
                hit = 0
                for i in range(len(result)):
                    if revdic[result[i]] in gold[total]:
                        hit += 1
                    total += 1
                precision = float(hit) / float(total)
                best_precision = max(best_precision, precision)
                print(
                    'epoch %i, minibatch %i/%i, test precision %f %%'%
                    (
                        epoch,
                        minibatch_index + 1,
                        n_train_batches,
                        precision * 100.
                    )
                )




    datasets = load_data('train.txt', vec_len, False)
    testsets = load_data('test.txt', vec_len, False)
    gold = load_gold('test.txt', vec_len)

    fff = open('train.txt')
    lll = fff.readlines()
    train_gold = []
    for t in lll:
        train_gold.append(labeldic[t.strip().split('\t')[-1]])
    
    train_set_x, train_set_y = datasets
    test_set_x, test_set_y = testsets

    n_train_batches = train_set_x.get_value(borrow = True).shape[0] // batch_size

    print('... building the model')

    rng = np.random.RandomState(9264326)

    classifier = MLP(
        rng = rng,
        input = x,
        n_in = vec_len,
        n_hidden = n_hidden,
        n_out = 11
    )

    cost = (
        classifier.negative_log_likelihood(y)
        + L1_reg * classifier.L1
        + L2_reg * classifier.L2_sqr
    )

    check_model = theano.function(
        inputs=[index],
        outputs=classifier.logRegressionLayer.y_pred,
        givens={
            x: train_set_x,
        },
        on_unused_input='ignore'
    )

    test_model = theano.function(
        inputs=[index],
        outputs=classifier.logRegressionLayer.y_pred,
        givens={
            x: test_set_x,
        },
        on_unused_input='ignore'
    )
    
    test_model0 = theano.function(
        inputs=[index],
        outputs=classifier0.logRegressionLayer.y_pred,
        givens={
            x: test_set_x,
        },
        on_unused_input='ignore'
    )

    gparams = [T.grad(cost, param) for param in classifier.params]

    updates = [
        (param, param - learning_rate * gparam)
        for param, gparam in zip(classifier.params, gparams)
    ]

    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )
    print('... training')

    test_frequency = n_train_batches

    best_precision = 0
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False

    while (epoch < n_epochs):
        epoch = epoch + 1
        for minibatch_index in range(n_train_batches):
            minibatch_avg_cost = train_model(minibatch_index)
            iter = (epoch - 1) * n_train_batches + minibatch_index
            if (iter + 1) % test_frequency == 0:
                result = test_model(0)
                result0 = test_model0(0)
                hit0 = 0
                for i in range(len(result0)):
                    if revdic[result0[i]] in gold[i]:
                        hit0 += 1
                res = []
                for i in range(len(result)):
                    tmp = revdic[result[i]]
                    if check(tmp):
                        tmp = revdic[result0[i]]
                    res.append(tmp)
                total = 0
                hit = 0
                for i in range(len(res)):
                    if res[i] in gold[total]:
                        hit += 1
                    total += 1
                if epoch == n_epochs:
                    if (os.path.exists('result')):
                        os.remove('result')
                    fout = open('result', 'a')
                    for tt in res:
                        fout.write(tt + '\n')
                    fout.close()
                precision = float(hit) / float(total)
                best_precision = max(best_precision, precision)
                print(
                    'epoch %i, minibatch %i/%i, test precision on dev %f %%' %
                    (
                        epoch,
                        minibatch_index + 1,
                        n_train_batches,
                        precision * 100.
                    )
                )

            if (iter + 1) % test_frequency == 0:
                result = check_model(0)
                total = 0
                hit = 0
                for i in range(len(result)):
                    if result[i] == train_gold[i]:
                        hit += 1
                    total += 1
                precision = float(hit) / float(total)
                print(
                    'epoch %i, minibatch %i/%i, check precision %f %%' %
                    (
                        epoch,
                        minibatch_index + 1,
                        n_train_batches,
                        precision * 100.
                    )
                )
                print 

    end_time = timeit.default_timer()
    print(('The code ran for %.2fm' % ((end_time - start_time) / 60.)))

    print 'Predicting the test_pdtb...'

    testsets = load_test('test_pdtb.txt', vec_len)

    test_model = theano.function(
        inputs=[index],
        outputs=classifier.logRegressionLayer.y_pred,
        givens={
            x: testsets,
        },
        on_unused_input='ignore'
    )
    
    test_model0 = theano.function(
        inputs=[index],
        outputs=classifier0.logRegressionLayer.y_pred,
        givens={
            x: testsets,
        },
        on_unused_input='ignore'
    )

    result = test_model(0)
    result0 = test_model0(0)
    res = []
    for i in range(len(result)):
        tmp = revdic[result[i]]
        if check(tmp):
            tmp = revdic[result0[i]]
        res.append(tmp)
    if (os.path.exists('test_result')):
        os.remove('test_result')
    fout = open('test_result', 'a')
    for tt in res:
        fout.write(tt + '\n')
    fout.close()



     

labeldic = {}
labeldic["Comparison.Concession"] = 0
labeldic["Expansion.Instantiation"] = 1
labeldic["Comparison.Contrast"] = 2
labeldic["Expansion.List"] = 3
labeldic["Expansion.Conjunction"] = 4
labeldic["Expansion.Alternative"] = 5
labeldic["Contingency.Cause"] = 6
labeldic["Contingency.Pragmatic cause"] = 7
labeldic["Temporal.Asynchronous"] = 8
labeldic["Expansion.Restatement"] = 9
labeldic["Temporal.Synchrony"] = 10

revdic = [0] * 11
revdic[0] = 'Comparison.Concession'
revdic[1] = 'Expansion.Instantiation'
revdic[2] = 'Comparison.Contrast'
revdic[3] = 'Expansion.List'
revdic[4] = 'Expansion.Conjunction'
revdic[5] = 'Expansion.Alternative'
revdic[6] = 'Contingency.Cause'
revdic[7] = 'Contingency.Pragmatic cause'
revdic[8] = 'Temporal.Asynchronous'
revdic[9] = 'Expansion.Restatement'
revdic[10] = 'Temporal.Synchrony'


test_mlp()
