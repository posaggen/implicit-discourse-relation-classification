import numpy

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams

from mlp import LogisticRegression, load_data, load_gold
from mlp import HiddenLayer
from rbm import RBM
import timeit


# start-snippet-1
class DBN(object):

    def __init__(self, numpy_rng, theano_rng=None, n_ins=784,
                 hidden_layers_sizes=[500, 500], n_outs=10):

        self.sigmoid_layers = []
        self.rbm_layers = []
        self.params = []
        self.n_layers = len(hidden_layers_sizes)

        assert self.n_layers > 0

        if not theano_rng:
            theano_rng = MRG_RandomStreams(numpy_rng.randint(2 ** 30))

        # allocate symbolic variables for the data
        self.x = T.matrix('x')  # the data is presented as rasterized images
        self.y = T.ivector('y')  # the labels are presented as 1D vector
                                 # of [int] labels
 

        for i in range(self.n_layers):
 
            if i == 0:
                input_size = n_ins
            else:
                input_size = hidden_layers_sizes[i - 1]

            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[-1].output

            sigmoid_layer = HiddenLayer(rng=numpy_rng,
                                        input=layer_input,
                                        n_in=input_size,
                                        n_out=hidden_layers_sizes[i],
                                        activation=T.nnet.sigmoid)

            self.sigmoid_layers.append(sigmoid_layer)

            self.params.extend(sigmoid_layer.params)

            rbm_layer = RBM(numpy_rng=numpy_rng,
                            theano_rng=theano_rng,
                            input=layer_input,
                            n_visible=input_size,
                            n_hidden=hidden_layers_sizes[i],
                            W=sigmoid_layer.W,
                            hbias=sigmoid_layer.b)
            self.rbm_layers.append(rbm_layer)

        self.logLayer = LogisticRegression(
            input=self.sigmoid_layers[-1].output,
            n_in=hidden_layers_sizes[-1],
            n_out=n_outs)
        self.params.extend(self.logLayer.params)

        self.finetune_cost = self.logLayer.negative_log_likelihood(self.y)


    def pretraining_functions(self, train_set_x, batch_size, k):

        index = T.lscalar('index')  # index to a minibatch
        learning_rate = T.scalar('lr')  # learning rate to use

        n_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
        batch_begin = index * batch_size
        batch_end = batch_begin + batch_size

        pretrain_fns = []
        for rbm in self.rbm_layers:

            cost, updates = rbm.get_cost_updates(learning_rate,
                                                 persistent=None, k=k)

            fn = theano.function(
                inputs=[index, theano.In(learning_rate, value=0.1)],
                outputs=cost,
                updates=updates,
                givens={
                    self.x: train_set_x[batch_begin:batch_end]
                }
            )
            pretrain_fns.append(fn)

        return pretrain_fns

    def build_finetune_functions(self, datasets, testsets, batch_size, learning_rate):

        train_set_x, train_set_y = datasets
        test_set_x, test_set_y = testsets

        index = T.lscalar('index')  # index to a [mini]batch

        gparams = T.grad(self.finetune_cost, self.params)

        updates = []
        for param, gparam in zip(self.params, gparams):
            updates.append((param, param - gparam * learning_rate))

        train_fn = theano.function(
            inputs=[index],
            outputs=self.finetune_cost,
            updates=updates,
            givens={
                self.x: train_set_x[
                    index * batch_size: (index + 1) * batch_size
                ],
                self.y: train_set_y[
                    index * batch_size: (index + 1) * batch_size
                ]
            }
        )

        test_model = theano.function(
            inputs=[index],
            outputs=self.logLayer.y_pred,
            givens={
                self.x: test_set_x,
            },
            on_unused_input='ignore'
        )

        return train_fn, test_model


def test_DBN(finetune_lr=0.1, pretraining_epochs=10,
             pretrain_lr=0.01, k=1, training_epochs=20,
             dataset='mnist.pkl.gz', batch_size=10):

    datasets = load_data('train.txt')
    testsets = load_data('test.txt')
    gold = load_gold('test.txt')
    
    train_set_x, train_set_y = datasets
    test_set_x, test_set_y = testsets

    n_train_batches = train_set_x.get_value(borrow = True).shape[0] // batch_size

    numpy_rng = numpy.random.RandomState(9264326)
    print '... building the model'
    dbn = DBN(numpy_rng=numpy_rng, n_ins=1200,
              hidden_layers_sizes=[400, 400, 400],
              n_outs=11)

    print '... getting the pretraining functions'
    pretraining_fns = dbn.pretraining_functions(train_set_x=train_set_x,
                                                batch_size=batch_size,
                                                k=k)

    print '... pre-training the model'
    start_time = timeit.default_timer()
    for i in range(dbn.n_layers):
        for epoch in range(pretraining_epochs):
            c = []
            for batch_index in range(n_train_batches):
                c.append(pretraining_fns[i](index=batch_index,
                                            lr=pretrain_lr))
            print 'Pre-training layer %i, epoch %d, cost ' % (i, epoch),
            print numpy.mean(c)

    end_time = timeit.default_timer()

    print '... getting the finetuning functions'
    train_fn, test_model = dbn.build_finetune_functions(
        datasets = datasets,
        testsets = testsets,
        batch_size = batch_size,
        learning_rate = finetune_lr
    )

    


    print '... finetuning the model'
    test_frequency = n_train_batches

    test_score = 0.
    start_time = timeit.default_timer()
    best_precision = 0
    epoch = 0

    while (epoch < training_epochs):
        epoch = epoch + 1
        for minibatch_index in range(n_train_batches):
            minibatch_avg_cost = train_fn(minibatch_index)
            iter = (epoch - 1) * n_train_batches + minibatch_index
            if (iter + 1) % test_frequency == 0:
                result = test_model(0)
                total = 0
                hit = 0
                for i in range(len(result)):
                    if revdic[result[i]] in gold[total]:
                        hit += 1
                    total += 1
                if epoch == training_epochs:
                    fout = open('result', 'a')
                    for tt in result:
                        fout.write(revdic[tt] + '\n')
                    fout.close()
                precision = float(hit) / float(total)
                best_precision = max(best_precision, precision)
                print(
                    'epoch %i, minibatch %i/%i, test precision %f %%' %
                    (
                        epoch,
                        minibatch_index + 1,
                        n_train_batches,
                        precision * 100.
                    )
                )

    end_time = timeit.default_timer()
    print(('The code ran for %.2fm' % ((end_time - start_time) / 60.)))


labeldic = {}
labeldic["Comparison.Concession"] = 0
labeldic["Expansion.Instantiation"] = 1
labeldic["Comparison.Contrast"] = 2
labeldic["Expansion.List"] = 3
labeldic["Expansion.Conjunction"] = 4
labeldic["Expansion.Alternative"] = 5
labeldic["Contingency.Cause"] = 6
labeldic["Contingency.Pragmatic cause"] = 7
labeldic["Temporal.Asynchronous"] = 8
labeldic["Expansion.Restatement"] = 9
labeldic["Temporal.Synchrony"] = 10

revdic = [0] * 11
revdic[0] = 'Comparison.Concession'
revdic[1] = 'Expansion.Instantiation'
revdic[2] = 'Comparison.Contrast'
revdic[3] = 'Expansion.List'
revdic[4] = 'Expansion.Conjunction'
revdic[5] = 'Expansion.Alternative'
revdic[6] = 'Contingency.Cause'
revdic[7] = 'Contingency.Pragmatic cause'
revdic[8] = 'Temporal.Asynchronous'
revdic[9] = 'Expansion.Restatement'
revdic[10] = 'Temporal.Synchrony'

test_DBN()

