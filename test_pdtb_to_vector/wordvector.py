import os
import shutil
import math


fin = open('words.txt')
fr = open('wordrule.txt')
if os.path.exists('wordvector.txt'):
    os.remove('wordvector.txt')
fout = open('wordvector.txt', 'a')

now = fin.readline()
rules = fr.readlines()
pool = []
while now != '':
    if now == '\n':
        cur = 0
        for i in rules:
            if (cur > 0):
                fout.write('\t')
            cur += 1
            if (i.strip() in pool):
                fout.write('1')
            else:
                fout.write('0')
        print cur
        fout.write('\n')
        pool = []
    else:
        pool.append(now.strip('\n'))
    now = fin.readline()

