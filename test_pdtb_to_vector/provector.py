import os
import shutil
import math

fin = open('tree.txt')
fr = open('prorule.txt')
if os.path.exists('provector.txt'):
    os.remove('provector.txt')
fout = open('provector.txt', 'a')

now = fin.readline()
rules = fr.readlines()
pool = []
arg1 = []
arg2 = []
while now != '':
    while not ('finished -----' in now):
        arg1.append(now.strip('\n'))
        now = fin.readline()
    now = fin.readline()
    while not ('finished -----' in now):
        arg2.append(now.strip('\n'))
        now = fin.readline()
    for i in arg1:
        pool.append(i + '1')
        if (i in arg2):
            pool.append(i + '3')
    for i in arg2:
        pool.append(i + '2')
    arg1 = []
    arg2 = []
    now = fin.readline()

    flag = True
    for i in rules:
        if not flag:
            fout.write('\t')
        flag = False
        if (i.strip() in pool):
            fout.write('1')
        else:
            fout.write('0')
    fout.write('\n')
    pool = []
    
