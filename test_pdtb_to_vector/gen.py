import os

fp = open('provector.txt')
fd = open('depvector.txt')
fw = open('wordvector.txt')
if os.path.exists('test_pdtb.txt'):
    os.remove('test_pdtb.txt')
fout = open('test_pdtb.txt', 'a')

l1 = fp.readlines()
l2 = fd.readlines()
l3 = fw.readlines()
for i in range(len(l1)):
    tmp = l1[i].strip() + '\t' + l2[i].strip() + '\t' + l3[i].strip()
    fout.write(tmp + '\n')
