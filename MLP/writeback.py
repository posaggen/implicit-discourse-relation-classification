import os
import json

def process():
    fin = open('dev_pdtb.json')
    if os.path.exists('gold.json'):
        os.remove('gold.json')
    if os.path.exists('predict.json'):
        os.remove('predict.json')
    f1 = open('gold.json', 'a')
    f2 = open('predict.json', 'a')
    f3 = open('result')
    L = fin.readlines()
    L2 = f3.readlines()
    cur = 0
    for i in L:
        j = json.loads(i)
        if j["Type"] == "Implicit":
            f1.write(i)
            j["Sense"] = [L2[cur].strip()]
            f2.write(json.dumps(j) + '\n')
            cur += 1

    print cur

process()
