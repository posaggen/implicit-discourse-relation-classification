import java.util.Collection;
import java.util.*;
import java.io.*;

import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.dcoref.*;
import edu.stanford.nlp.hcoref.CorefSystem;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.TreeBinarizer;

class ParserDemo {

	public static void main(String[] args) throws Exception {
		String parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
		if (args.length > 0) {
			parserModel = args[0];
		}
		LexicalizedParser lp = LexicalizedParser.loadModel(parserModel);
		demoDP(lp, "pdtb.txt");
	}
/*	
	static void runCoref() throws Exception{
		SieveCoreferenceSystem scfs = new SieveCoreferenceSystem(new Properties());
		MentionExtractor me = new MentionExtractor(scfs.dictionaries(), scfs.semantics());
		String line = sentenceFile.readLine();
		Tree parse;
		int round = 0;
		while (line != null) {
			PrintWriter ifP = new PrintWriter(new FileWriter("if"));
			ifP.println(line);
			ifP.close();
			System.err.println(++round);
			for (List<HasWord> sentence : new DocumentPreprocessor("if")) {
				parse = lp.apply(sentence);

			}
	}
*/
	static void show(Tree now, PrintWriter treeP) {
		Tree[] children = now.children();
		if (children.length == 0)
			return;
		String tmp = now.label().value();
		for (Tree element : children) {
			tmp += "," + element.label().value();
			show(element, treeP);
		}
		treeP.println(tmp);
	}

	public static void demoDP(LexicalizedParser lp, String filename) throws Exception {
		//System.setOut(new PrintStream("./output"));
		BufferedReader sentenceFile = new BufferedReader(new FileReader(filename));
		PrintWriter treeP = new PrintWriter(new FileWriter("tree.txt"));
		PrintWriter dependencyP = new PrintWriter(new FileWriter("dependency.txt"));
		String line = sentenceFile.readLine();
		Tree parse;
		int round = 0;
		while (line != null) {
			PrintWriter ifP = new PrintWriter(new FileWriter("if"));
			ifP.println(line);
			ifP.close();
			System.err.println(++round);
			for (List<HasWord> sentence : new DocumentPreprocessor("if")) {
				parse = lp.apply(sentence);
				TreebankLanguagePack tlp = lp.treebankLanguagePack();
				GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
				GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
				List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();
				Collections.sort(tdl, new Comparator<TypedDependency>() {
					public int compare(TypedDependency u, TypedDependency v) {
						return u.gov().compareTo(v.gov());
					}
				});
				String last = "", tmp = "";
				List<String> list = new ArrayList<String>();
				for (TypedDependency element : tdl) {
					// System.out.println(element.toString());
					String t = element.gov().value();
					if (t.equals(last)) {
						list.add(element.reln().toString());
					} else {
						if (tmp != "") {
							Collections.sort(list);
							String tt = "";
							for (String u : list) {
								if (u.equals(tt))
									continue;
								tt = u;
								tmp = tmp + "," + u;
							}
							// System.out.println(tmp);
							dependencyP.println(tmp);
						}
						tmp = t;
						list.clear();
						list.add(element.reln().toString());
						last = t;
					}
					// System.out.println(element.gov().toString());
				}
				// if (tmp != "") System.out.println(tmp);
				if (tmp != "") {
					Collections.sort(list);
					String tt = "";
					for (String u : list) {
						if (u.equals(tt))
							continue;
						tt = u;
						tmp = tmp + "," + u;
					}
					dependencyP.println(tmp);
				}
				// System.out.println();
				// System.out.println(tdl);
				// System.out.println();
				show(parse, treeP);
				/*TreeBinarizer tb = new TreeBinarizer(lp.getTLPParams().headFinder(), tlp, false, false, round, false,
						false, round, false, false, false);

				TreePrint tp = new TreePrint("penn");
				Tree bb = tb.transformTree(parse);*/
				/*show(bb, treeP);
				tp.printTree(parse);*/
				// tp.printTree(bb);
				// System.out.println();
				/*
				 * TreePrint tp = new TreePrint("typedDependenciesCollapsed");
				 * tp.printTree(parse);
				 */
			}
			dependencyP.println("Round: " + Integer.toString(round) + " finished -----");
			treeP.println("Round: " + Integer.toString(round) + " finished -----");
			line = sentenceFile.readLine();
		}
		sentenceFile.close();
		treeP.close();
		dependencyP.close();
	}

	private ParserDemo() {
	} // static methods only

}
