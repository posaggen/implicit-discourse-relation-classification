Three different implicit discourse relation classifier using Max Entropy Model, Deep Belief Network and Multilayer Perceptron, a neural network with three fully-connected hidden layer.

The data set used is Penn Discourse Treebank(PDTB). The Multilayer Perceptron shows the best accuracy: 46.01%.